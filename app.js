/**
 * @title Narrow Node Portal
 * @description The portal site for narrownode.org, containing Narrow Node projects, team members, and development panel.
 * @author ethancrist
 **/
 
// [DEPENDENCIES]
const model = { cdn: '/res/', views: 'views', cache: '?_='+Date.now() }
const micro = require('iomicro')
const request = require('needle')
const nodesAPIUrl = 'http://localhost:3001/'

// [[MIDDLEWARE]]
micro.use(require('narrownode-webcore'))
micro.use((req, res, next) => {
	res.setHeader('Content-Security-Policy', '*')
	next()
})


// [[ENDPOINTS]]
// Serving '/res' files.
micro.static(model.cdn, model.views+'/res/')

micro.post('/create', (req, res) => {
	/**
	 * @purpose Request the Nodes API to create a new Node.
	 **/
	
	// Calling User's API...
	request.post(nodesAPIUrl + 'nodes', req.body, function(err, apiRes) {
		res.setHeader('Content-Type', 'application/json')
		res.status(apiRes.statusCode).send(apiRes.body)
	})	
})

micro.post('/gimme', (req, res) => {
	/**
	 * @purpose Request the Nodes API to fetch all nodes.
	 **/
	
	// Calling Nodes API...
	request.get(nodesAPIUrl + 'nodes', function(err, apiRes) {
		res.setHeader('Content-Type', 'application/json')
		res.status(apiRes.statusCode).send(apiRes.body)
	})	
})

micro.post('/get-schema', (req, res) => {
	/**
	 * @purpose Request the Nodes API to fetch its schema.
	 **/
	
	// Calling Nodes API...
	request.get(nodesAPIUrl + 'nodes/schema', function(err, apiRes) {
		res.setHeader('Content-Type', 'application/json')
		res.status(apiRes.statusCode).send(apiRes.body)
	})	
})

micro.post('/node-info', (req, res) => {
	/**
	 * @purpose Request the Nodes API to retrieve a Node's metadata.
	 **/
	
	// Calling Nodes API...
	request.get(nodesAPIUrl + 'nodes/' + req.body.node, function(err, apiRes) {
		res.setHeader('Content-Type', 'application/json')
		res.status(apiRes.statusCode).send(apiRes.body)
	})	
})

micro.post('/delete-node', (req, res) => {
	/**
	 * @purpose Request the Nodes API to delete a Node entry.
	 **/
	
	// Calling Nodes API...
	request.delete(nodesAPIUrl + 'nodes/' + req.body.name, null, null, function(err, apiRes) {
		res.setHeader('Content-Type', 'application/json')
		res.status(apiRes.statusCode).send(apiRes.body)
	})	
})

micro.post('/node-status', (req, res) => {
	/**
	 * @purpose Request the Nodes API to get a Node' online status.
	 **/
	
	// Calling Nodes API...
	request.get(nodesAPIUrl + 'nodes/' + req.body.name + '/status', function(err, apiRes) {
		res.setHeader('Content-Type', 'application/json')
		res.status(apiRes.statusCode).send(apiRes.body)
	})	
})

micro.get('*', (req, res) => {
        res.removeHeader('X-Content-Type-Options')

        // view = 'home', 'dev', 'docs', etc (determined automatically)
        const view = req.query.dynamic !== 'true' ? 'global' : req.originalUrl.substring(1).split('?')[0]
        res.render(view === '' ? 'home' : view, model)
})

// [START]]
micro.listen(5000, {
    appName: 'Narrow Node - Portal'/*,
    ssl: {
        key: '/etc/letsencrypt/live/narrownode.org/privkey.pem',
        cert: '/etc/letsencrypt/live/narrownode.org/fullchain.pem'
    }
    */
})
