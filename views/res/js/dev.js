(function() {

const loading = (toggle) => {
    /**
     * @purpose Toggle the loading graphic.
     **/
    toggle ? $('#swirl').show() : $('#swirl').hide()

    $('#check').attr('disabled', toggle)
}
const ping = (url, callback) => {
    /** 
     * @purpose Ping a server, and get its ms response.
     * @return The ms number, or -1 if the server is offline and gave no response.
     **/

    // TODO: Ping server
    callback(Core.random(3) === 1 ? -1 : Core.random(101))
}
const pingServers = () => {
    /**
     * @purpose Ping for object of servers to check if they are online.
     */

    // TODO: Get application list
    let servers = { 
        game: true,
        site: false,
        database: false,
        api: true
    }

    // Clearing out list from previous pings...
    $('#status-list').html('')

    // Filling out new server list based on its response..
    Object.keys(servers).forEach((key) => {
        ping(servers[key].url, (ping) => {
            $('#status-list').html($('#status-list').html()+'<li id="server-'+key+'" class="'+(ping > -1 ? 'online' : 'offline')+'">'+key+(ping > -1 ? ' ('+ping+'ms)' : '')+'<span class="restart">⟲</span></li>')
        })
    })
}
const eventListeners = () => {
    /**
     * @purpose Listen for certain events.
     * @justify These are sometimes lost due to scope.
     **/

    // Removing all previous click listeners to avoid potential duplicates.
    $('.online').off('click')
    $('.restart').off('click')

    // Adding click listener for online servers within the list.
    $('.online').click((e) => { showServerInfo(e.target.id.substring(7)) })

    // Adding listener for restart button for offline servers.
    $('.restart').click((e) => {
        // Cancel operation if already restarting
        if ($(e.target).hasClass('restarting')) return

        const server = $(e.target).parent().attr('id').substring(7)
        if (confirm('Restart '+server+'?')) restart(server)
    })
}
const check = () => {
    /**
     * @purpose Check the status of the servers.
     **/

    // Cancel operation if already checking
    if ($('#check').attr('disabled')) return
    
    // Loading icon while it's checking...
    loading(true)

    // Refreshing the data if it's shown...
    const currentServer = $('#focus').html()
    if (!Core.isEmpty(currentServer)) showServerInfo(currentServer)

    // Making sure the status time is shown... (on page load it's not cause it has no data)
    $('#checkers').show()

    // Getting server list...
    let servers = pingServers()

    // Updating the time it last checked now that it's done.
    $('#check-time').html(new Date().toLocaleString())

    // Done; removing loading icon.
    loading(false)

    eventListeners()
}
const restart = (server) => {
    /**
     * @purpose Send a request to restart a server.
     **/

    // Basic interfacing...
    $('#server-'+server).addClass('restarting')
    $('#server-'+server).html('Restarting '+server+'...')

    // TODO: Send restart request to API

    check()
}
const getServerInfo = (server, callback) => {
    /**
     * @purpose Request all metadata for a server from the API.
     **/
    // TODO: Pull logs and other shit here, pull from API I suppose
    const metadata = { name: server }

    callback(metadata)
}
const showServerInfo = (server) => {
    /**
     * @purpose Show all metadata for an online server, including logs, uptime, users online, etc..
     **/
    $('#data').show()
    $('#data *').hide()
    $('#loading-data').show()

    // Filling out server info from the API..
    getServerInfo(server, (data) => {
        $('#data *').show()
        $('#loading-data').hide()
        $('#focus').html(data.name)
        $('#logs').html(Date.now())
    })
}

// [[LANDING]]

// "Check" button forces status check
check()
$('#check').click(check)
})()
