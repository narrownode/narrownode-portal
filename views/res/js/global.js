/**
 * @title Narrow Node Portal - Global JS
 * @description JS script that runs on every page of the Narrow Node portal.
 * @author ethancrist
 **/

// [[DEPENDENCIES]]
const audio = { }
var cookie = ''
const nodesListElement = '#create-new-node, .nodes-list'
var originalNodesList

// [[FUNCTIONS]]
const load = () => {
	/**
	 * @purpose Preload content for the page.
	 */
	
	return new Promise((resolve, reject) => {
		audio.intro = new Audio(Core.cdn+'sfx/intro.wav'+Core.cache)
		audio.link = new Audio(Core.cdn+'sfx/link.wav'+Core.cache)
		resolve()
	})
}
const toggleMainMenu = () => {
	/**
	 * @purpose Toggle whether or not the menu is the main menu or a sub-menu.
	 */

	// Determining true value of 'toggle' based on the webpage.
	const toggle = location.pathname === '/'

	// Playing SFX for intro transition/link click...
	audio[toggle ? 'intro' : 'link'].play()

	// Removing potential previous duplicate class instances on #menu.
	$('#menu').removeClass('main')
	$('#menu span').removeClass('selected-link')

	if (toggle) {
		// Adding a main class if #menu is the main menu.
		$('#menu').addClass('main')
		$('#home').removeClass('__webcore-a')

		$('#outer-content .content').hide()
	} else {
		// Highlighting the menu option that is active based on the page loaded...
		const menuOption = location.pathname.substring(1)
		$('#'+menuOption).addClass('selected-link')

		$('#home').addClass('__webcore-a')
		$('#outer-content .content').show()
	}
	
	// Showing the menu, because on page load it is hidden...
	$('#menu').show();
}
const toggleControlPanel = () => {
	/**
	 * @purpose Toggle whether or not the Mainframe Control Panel overlay is visible.
	 */

	// Determining true value of 'toggle' based on the webpage.
	const toggle = $('#control-panel').css('display') === 'none'

	// Playing SFX for intro transition/link click...
	audio[toggle ? 'intro' : 'link'].play()

	// Toggling the view of the control panel...
	$('#control-panel')[toggle ? 'show' : 'hide']();

	// Refreshing nodes list...
	fetchNodes()
}
const imageToBase64 = (imageFile) => {
	/**
	 * @purpose Convert an image to base64 String.
	 * @return A Promise callback to run after the process is complete.
	 */
	return new Promise(function(resolve, reject) {
		const reader = new FileReader()
	
		reader.onload = function() {
			// Running implementation success callback if applicable...
			resolve(reader.result)
		}

		reader.onerror = function() {
			// Running implementation error callback if applicable...
			reject(reader.error)
		}
		
		reader.readAsDataURL(imageFile)
	})
}
const mfcpTabs = (e) => {
	/**
	 * @purpose Swap between Mainframe Control Panel tabs.
	 */

	// Determining which tab was clicked...
	const tabClicked = e.target.id

	// Playing SFX for intro transition/link click...
	audio.link.play()

	// Resetting all active tabs to avoid duplicate activation...
	$('.mfcp-menu').removeClass('selected-link')	

	// Activating relevant MFCP tab...
	$('#'+tabClicked+'.mfcp-menu').addClass('selected-link')	

	// Hiding all MFCP content to reset...
	$('#mfcp-content').children().hide();
	
	// Showing relevant MFCP content based on which tab was clicked...
	$('#content-'+tabClicked).show()
}
const isJQueryObject = (object) => {
	/**
	 * @purpose Determine if the object passed is a jQuery DOM object.
	 * @return Boolean of the result determined.
	 **/
	return object.target && Object.keys(object.target)[0].substring(0,6) === 'jQuery' ? true : false
}
const editNode = (nodeData) => {
}
const deleteNode = (nodeData) => {
	/**
	 * @purpose Send a call to the Nodes API to delete the Node selected.
	 **/

	// Safeguarding for misclicks by asking the user if they are sure they want to continue...
	if (!confirm('Are you sure? Deleting is irreversible.')) return
	
	// If the nodeData variable is a jQuery object from clicking the Delete Node button,
	// then reformat the nodeData variable into a parsable jsonObject to send to the API.
	
	if (isJQueryObject(nodeData)) nodeData = { name: $('#node-field-name .node-value').html() }

	Core.post({ url: '/delete-node', body: nodeData }).done(function(res, textStatus, metaData) {
		// Node successfully deleted; returning to the Create New Node pane and refreshing all nodes.
		// Note: This means that when a Node is deleted without errors, the success message for the
		// deletion will show above the Create New Node pane.
		
		if (metaData.status === 200) {
			fetchNodes()
			$('#create-new-node').click()
		}
		
		displayResponseMessage(res.message, metaData.status)
	}).fail(function(err) {
		displayResponseMessage(err.responseText.message, err.status)
	})
}
const getInputType = (dataType) => {
	/**
	 * @purpose Get the best HTML input type based on a data type.
	 * @return The HTML input type.
	 **/
	switch (dataType) {	
		case 'Boolean':
			return 'checkbox'
			
		case 'Array':
			return 'select'
			
		case 'Image':
			return 'file'
	
		case 'String':
		default:
			return 'text'
	}
}
const toggleNewNodePreferences = (field) => {
	/**
	 * @purpose Toggle the showing of a field's preferences (AKA sub-options) when creating a new node based on whether or not the field is enabled.
	 * @parameter field
	 * 		The parent field to which the preferences apply.
	 */
	$('.node-preference.preference-for-field-'+field)[$('#new-node-'+field).is(':checked') ? 'show' : 'hide']()
}
const fillNodeContentHTML = (node) => {
	/**
	 * @purpose Auto-create the HTML of the Node content area on the Mainframe Control Panel, based on a particular Node's data.
	 **/

	// Making a call to the Nodes API to get the Node schema.
	Core.post({ url: '/get-schema', cookie: cookie }).then((apiRes) => {
		// Refreshing Node content area...
		// Adding widgets separate from Node data that still lie within the Node content area...
		$(node == 'Create-New-Node' ? '#create-new-node-options' : '#node-options').show()
		$('#create-new-node-content, #mfcp-node-content').html('')
		$('#view-node-field-logo').attr('src', '')
	
		const nodeData = { node: node, schema: apiRes }

		// Here we will HTMLize the Node entry based on the Nodes API Node schema.
		Object.keys(nodeData.schema).forEach(function(field) {
			// Note:
			// 		field == The key to each Node schema object field. (i.e. "name", "description", etc.)
			// 		nodeData.node[field] == The value to each Node entry object field. (i.e. <"Name of App">, <"Description of app">, etc.)
			//		nodeData.schema[field] = The value to each Node schema object field. (i.e. "type", "required", "default", etc)
			
			// If the Node field being parsed is a logo, then add it to the image source tag.
			if (node !== 'Create-New-Node' && field === 'logo') return $('#view-node-field-logo').attr('src', nodeData.node[field]).show()
			
			const newOrExistingNode = (node === 'Create-New-Node' ? 'new-node' : 'node')
		
			var nodeFieldHTML =
				`<div id="node-field-`+field+`" class="`+newOrExistingNode+`-field" data-schema="`+nodeData.schema[field].type+`">` +
					
					(node === 'Create-New-Node' ?
						// New Node value field
						`<label>`+field+`</label>:
						<input id="new-node-`+field+`" class="new-node-value" data-schema="`+nodeData.schema[field].type+`"
							type="`+getInputType(nodeData.schema[field].type)+`" ` +
							
							// If there are field preferences:
							//		onclick="toggleNewNodePreferences()"
							// otherwise, don't add this parameter
							(nodeData.schema[field].preferences ?
								`onchange="toggleNewNodePreferences('`+field+`')"` : ``)+ `></input>` :
							
						// Existing Node value field
						`<span class="node-key">`+field+`</span>:
						<span class="node-value">`+nodeData.node[field]+`</span>`
					
					) +
					
				`</div>`
		
			// If this field of Node data has sub-options, i.e. "preferences", load them.
			// For example, in the Node schema, the field "website" dictating whether the Node has a website,
			// has the sub-options "url" and "theme" that are sub-options of "website".
			// Most fields do not have sub-options hence the distinction.
			if (nodeData.schema[field].preferences) {
								
				Object.keys(nodeData.schema[field].preferences).forEach(function(preference) {
					
					const subFieldType = nodeData.schema[field].preferences[preference].type
					
					let newNodePreferenceValue = `<input id="new-node-`+preference+`" class="new-node-value" type="`+getInputType(subFieldType)+`"></input>`
					
					if (subFieldType === 'Array') {
						// Preference has Array of sub-options; need to auto-fill a select box.
						const preferenceSubOptions = nodeData.schema[field].preferences[preference].options
						let preferenceSubOptionsHTML = ``
						
						// Looping through all these sub-options to create lines of HTML for each option...
						preferenceSubOptions.forEach((option) => {
							preferenceSubOptionsHTML += `<option value="`+option+`">`+option+`</option>`
						})									
									
						newNodePreferenceValue = 
							`<select id="new-node-`+preference+`" class="new-node-value" type="`+getInputType(subFieldType)+`">` +
									preferenceSubOptionsHTML +
							`</select>`
					}
					
					nodeFieldHTML +=
						`<div id="node-preference-`+preference+`" class="preference-for-field-`+field+` `+newOrExistingNode+`-field node-preference" data-schema="`+subFieldType+`">` +							
							
							(node === 'Create-New-Node' ?
								// New Node value field
								`<label>`+preference+`</label>:`+
								newNodePreferenceValue :

								// Existing Node value field
								`<span class="node-key">`+preference+`</span>:
								<span class="node-value">`+nodeData.node[preference]+`</span>`
							
							) +
							
						`</div>`
						
						
				})
			}
			
			// If the Node field is NOT A logo, then fill in the node key as a label and the value afterwards.
			$(node ==='Create-New-Node' ? '#create-new-node-content' : '#mfcp-node-content')[0].innerHTML += nodeFieldHTML
			
		})
		
		// CLICK LISTENERS
		if (node === 'Create-New-Node') {
			// Create New Node
			
			// When the Browse... image is changed on the Create New Node pane, the code below will run.
			// Also removing any potential pre-existing duplicate listeners.
			$('#new-node-logo').unbind('change')
			$('#new-node-logo').on('change', function() {
				const imageFile = document.querySelector('#new-node-logo')['files'][0]
	
				imageToBase64(imageFile).then((res) => {
					// Process successful; saving the base64 onto the DOM element with an attribute name 'base64' so it may be statelessly accessed later.
					$(this).attr('base64', res)
				}).catch((err) => {
					// An error occurred; an error message will be displayed.
					displayResponseMessage(err, 500)
				})
			})
		} else {
			// View Node
				
			// Adding click listeners for buttons within the Node content area...
			// Also removing any potential pre-existing listeners beforehand...
			$('#edit-node').unbind('click')
			$('#delete-node').unbind('click')
			$('#edit-node').click(editNode)
			$('#delete-node').click(deleteNode)
		}
	
	}).fail((err) => {
		displayResponseMessage(err, 500)
	})

}
const mfcpNodes = (e) => {
	/**
	 * @purpose Swap between Mainframe Control Panel nodes.
	 */

	// Determining which node was clicked...
	// The conditional is to make sure the element inside the .nodes-list wasn't clicked.
	const elementClicked = $('[id="'+e.target.id+'"]') // Using this instead of $('#'+e.target.id) to safeguard for IDs that have spaces in them.
	let nodeClicked = elementClicked.hasClass('nodes-list') ? e.target.id : elementClicked.parent().attr('id')

	if (e.target.id === 'create-new-node') nodeClicked = e.target.id

	// Playing SFX for intro transition/link click...
	audio.link.play()

	// Resetting all active node HTML content to avoid duplicate activation...
	$('#mfcp-right-side').children().hide()
	
	// Removing potential duplicates...
	$('.nodes-list-name').removeClass('selected-link')

	// Adding a class to this link so that it appears as currently selected in the interface.
	$('[id="'+nodeClicked+'"] [class="nodes-list-name"]').addClass('selected-link')

	if (nodeClicked === 'create-new-node') {
		// Create New Node was clicked in the list.

		// Showing Create New Node submission form...
		$('#create-new-node-content').show()
		
		// Filling the Create New Node content HTML...
		fillNodeContentHTML('Create-New-Node')

	} else {
		// Existing Node was clicked in the list.

		// Showing metadata of existing Node...
		$('#mfcp-node-content').show()

		// Only making an API call to GET Node if an existing Node was clicked in the list.
		const data = { node: nodeClicked.substring(5), cookie: cookie }
		Core.post({ url: '/node-info', body: data }).then((nodeData) => {
			fillNodeContentHTML(nodeData)

			// Unbinding pre-existing click-listeners for this element to avoid potential duplicates.
			$(nodesListElement).unbind('click')
			$(nodesListElement).click(mfcpNodes)
		}).fail((err) => {
			displayResponseMessage(JSON.stringify(err), 500)
		})
	}
	
}
const getStatus = (node) => {
	/**
	 * @purpose Get the online/offline status of a Node.
	 * @return Promise of the API call for GET Status.
	 **/
	
	return Core.post({ url: 'node-status', body: { name: node } })
}
const updateNodesHTML = (nodesList) => {
	/**
	 * @purpose Change the HTML interface based on the updated nodes list.
	 */

	// Removing potential pre-existing duplicates...
	$('.status-bubble, .nodes-list').remove()

	// Adding HTML list additions for each Node from the nodesList
	nodesList.forEach((node) => {
		$('#mfcp-nodes-list').append(`<li id="node-`+node+`" class="nodes-list">
					      	<span id="node-status-`+node+`" class="status-bubble __webcore-center-page"></span>
					      	<span id="node-name-`+node+`" class="nodes-list-name">`+node+`</span>
					      </li>`)

		getStatus(node).then((res) => {
			// Retrieved status of the Node; updating the HTML for it...
			$('[id="node-status-'+node+'"]').addClass('status-'+res.status)
		}).fail((err) => {
			// Could not get status; defaulting to offline status.
			$('[id="node-status-'+node+'"]').addClass('status-offline')
		})
	})

	// Resetting click listener...
	$(nodesListElement).unbind('click')
	$(nodesListElement).click(mfcpNodes)
}

const fetchNodes = () => {
	/**
	 * @purpose Call the Nodes API to retrieve node data and populate it on the Nodes setcions.
	 */
	const data = { cookie: cookie }
	Core.post({ url: '/gimme', data }).then((nodes) => {
		originalNodesList = nodes
		updateNodesHTML(nodes)

		$(nodesListElement).unbind('click')
		$(nodesListElement).click(mfcpNodes)
	})
}
const getCreateNewNodeData = () => {
	/**
	 * @purpose Parse the data of the input boxes on the Create New Node panel into a sendable format.
	 * @return JSON object of the request body parameters.
	 */
	 
	 const data = {
		'birth': Date.now(),
		'cookie': cookie
	}

	// Auto-filling data for the new Node based on the fields filled out.
	Object.keys($('.new-node-value')).forEach(function(valueIndex) {		
		const element = $('.new-node-value')[valueIndex]
		
		// Making sure the object index in question is actually an HTML element...			
		if (element.id === undefined) return
		
		// id="new-node-*"
		const newNodeKey = element.id.substring(9)
		
		function parseElement(e) {
			/**
			 * @purpose Parse the data of the input boxes on the Create New Node panel into a sendable format.
			 */
			 
			 switch ($(e).attr('data-schema')) {
				// File
				case 'Image': 
					return $(e).attr('base64')
					
				// Checkbox
				case 'Boolean': 
					return $(e).is(':checked')
				
				// Select dropdown
				case 'Array': 
					return $(e).val().split(',')
				
				// Text / all other input types
				default: 
					return $(e).val()
			 }
		}
		
		data[newNodeKey] = parseElement(element)
	})
	
	return data
}
const submitNewNode = () => {
	/**
	 * @purpose Create new node based on the data the user entered on the form.
	 */
	
	displayResponseMessage('Loading...')

	Core.post({ url: '/create', body: getCreateNewNodeData() }).done(function(res, textStatus, jqXHR) {
		// jqXHR = https://www.w3schools.com/XML/dom_http.asp
		// If Node was successfully created (201 code), then refresh list of nodes.
		if (jqXHR.status === 201) {
			res = '"'+res.name+'" Node was successfully created!'
			fetchNodes()
		}

		displayResponseMessage(res, jqXHR.status)
	}).fail(function(err) {
		displayResponseMessage(err.responseText, err.status)
	})
}

const filterSearch = (searchList, phrase) => {
	/**
	 * @purpose Filter result list from a search array.
	 * @return Filtered results.
	 **/	
	phrase = phrase || $('#mfcp-search-nodes').val()

	let resultList = []
	searchList.forEach(function(result) {
		if (result && result.toLowerCase().includes(phrase.toLowerCase())) resultList.push(result)
	})
	return resultList
}

const filterNodesHTML = () => {
	/**
	 * @purpose Change HTML of the Mainframe Control Panel nodes list based on the filter of the search box.
	 **/	
	updateNodesHTML(filterSearch(originalNodesList))
}

const refreshListeners = () => {
	/**
	 * @purpose Refresh list of listeners to the event queue.
	 **/
	
	// Removing previous listeners to avoid duplicates...
	const listeners = ['#menu span, .__webcore-a', '#login', '.mfcp-menu', '#new-node-submit']
	listeners.forEach(function(listener) {
		//$(listener).unbind('click')
	})

	// Adding list of listeners..
	$('#menu span, .__webcore-a').click(toggleMainMenu)
	$('#login').click(toggleControlPanel)
	$('.mfcp-menu').click(mfcpTabs)
	$('#new-node-submit').click(submitNewNode)
	$('#mfcp-search-nodes').on('keyup', filterNodesHTML)
}

const removeClassBySubstring = (element, substring) => {
	/**
	 * @purpose Pass through a partial string of a class name and remove all instances of the entire class containing the substring within an element.
	 * TODO: Add this to Webcore
	 **/
	
	// We will begin by defining this variable as the entire class string.
	// i.e. "response-code-INFO response-code-WARNING response-code-SUCCESS"
	var oldClassString = element.attr('class')

	// No previous classes exist; abort.	
	if (!oldClassString) return

	let newClassString = ''
	oldClassString.split(' ').forEach(function(c) {
		// Removing all classes whose name contain the passed substring...
		if (!c.includes(substring)) newClassString += ' '+c
	})

	// Reset element with new, edited classlist.
	element.attr('class', newClassString)
}

const displayResponseMessage = (message, code) => {
	/**
	 * @purpose Display SUCCESS (2xx), INFO(3xx), WARNING(4xx), and ERROR(500+) messages.
	 **/

	// Default
	codeName = 'INFO'
	if (code >= 200 && code <= 299) codeName =　'SUCCESS'
	if (code >= 300 && code <= 399) codeName =　'INFO'
	if (code >= 400 && code <= 499) codeName =　'WARNING'
	if (code >= 500) codeName =　'ERROR'

	// Removing previous response type styling...
	removeClassBySubstring($('#nodes-response'), 'response-code-')

	// Adding response type styling...
	$('#nodes-response').addClass('response-code-'+codeName)

	// Changing the response type message...
	$('#nodes-response-message').html(codeName + ': ' + message)
	$('#nodes-response').show()
}

// [[LANDING]]
load().then((success) => {
	toggleMainMenu()
	refreshListeners()
}, (err) => {
	alert('Could not load resources, please refresh.\nError: '+err)
})
